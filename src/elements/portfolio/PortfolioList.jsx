import React, { Component } from "react";
import {Link} from "react-router-dom";

const PortfolioListContent = [
    {
        image: 'image-1',
        category: 'Development',
        title: 'Diário Oficial do Município de Anápolis'
    },
    {
        image: 'image-2',
        category: 'Development',
        title: 'Meu Lote Minha História'
    },
    {
        image: 'image-3',
        category: 'Development',
        title: 'Sistema de controle de doações'
    },
    {
        image: 'image-4',
        category: 'Development',
        title: 'Controle de vacinação'
    },
    {
        image: 'image-3',
        category: 'Development',
        title: 'Sistema de Eleição de diretores da rede municipal de educação'
    },
    {
        image: 'image-4',
        category: 'Development',
        title: 'Sistema de doações de bolsas de estudo'
    }
]

class PortfolioList extends Component{
    render(){
        const {column , styevariation } = this.props;
        const list = PortfolioListContent.slice(0 , this.props.item);
        return(
            <React.Fragment> 
                {list.map((value , index) => (
                    <div className={`${column}`} key={index}>
                        <div className={`portfolio ${styevariation}`}>
                            <div className="thumbnail-inner">
                                <div className={`thumbnail ${value.image}`}></div>
                                <div className={`bg-blr-image ${value.image}`}></div>
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <p>{value.category}</p>
                                    <h4><a href="/portfolio-details">{value.title}</a></h4>
                                    <div className="portfolio-button">
                                        <a className="rn-btn" href="/portfolio-details">Veja detalhes</a>
                                    </div>
                                </div>
                            </div>
                            <Link className="link-overlay" to="/portfolio-details"></Link>
                        </div>
                    </div>
                ))}
               
            </React.Fragment>
        )
    }
}
export default PortfolioList;